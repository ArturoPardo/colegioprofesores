package EjercicoColegio;

import java.util.Scanner;

public class Colegio {

	private Asignaturas[] asignaturas;
	private Profesores[] profesores;
	private Alumnos[] alumnos;

	private int contadorProfesores;
	private int contadorAsignaturas;
	private int contadorAlumnos;

	private Scanner sc;

	public Colegio() {

		this.asignaturas = new Asignaturas[20];
		this.profesores = new Profesores[10];
		this.alumnos = new Alumnos[20];

		this.contadorAsignaturas = 0;
		this.contadorProfesores = 0;
		this.contadorAlumnos = 0;

	}

	public void mostrarProfes() {
		System.out.println(contadorProfesores);
		System.out.println(profesores.length);
		for (int i = 0; i < profesores.length; i++) {
			String seleccion = profesores[i].getNombre();
			if (seleccion != null) {

				System.out.println(seleccion);
			}

		}

	}

	public void run() {
		sc = new Scanner(System.in);
		System.out.println("Bienvenido al gestor de colegios.\n");
		System.out.println("¿Que opción quieres elegir?.\n");
		System.out.println(
				" 1.- Crear Asignaturas\n 2.- Crear Profesores\n 3.- Crear alumnos\n 4.- Vincular asignaturas\n 5.- Salir\n Escribe tu selección y pulsa la tecla INTRO");

		int mensajeMain = sc.nextInt();

		if (mensajeMain == 1) {
			this.crearAsignaturas();
		}
		if (mensajeMain == 2) {
			this.crearProfesores();
		}
		if (mensajeMain == 3) {
			this.crearAlumnos();
		}
		if (mensajeMain == 4) {
			this.vincularAsignaturas();
		}
		if (mensajeMain == 5) {
			this.salir();
		} else {
			this.run();
		}

	}

	public void crearAsignaturas() {
		if (contadorAsignaturas <= asignaturas.length) {

			System.out.println("Creando asignaturas ..");
			System.out.println("Introduce nombre");
			String nombre = sc.next();
			System.out.println("Introduce siglas");
			String siglas = sc.next();
			System.out.println("Introduce número de horas lectivas");
			int horasLectivas = sc.nextInt();

			this.asignaturas[contadorAsignaturas] = new Asignaturas(nombre, siglas, horasLectivas);
			contadorAsignaturas++;

		} else {
			System.out.println("Asignaturas Completas");

		}
	}

	public void crearProfesores() {

		if (contadorProfesores <= profesores.length) {

			System.out.println("Creando profesores ..");
			System.out.println("Introduce nombre");
			String nombre = sc.next();
			System.out.println("Introduce apellidos");
			String apellidos = sc.next();
			System.out.println("Introduce sexo");
			String sexo = sc.next();

			System.out.println("Introduce edad");
			int edad = sc.nextInt();
			System.out.println("Introduce dni");
			String dni = sc.next();
			this.profesores[contadorProfesores] = new Profesores(nombre, apellidos, sexo, edad, dni);
			contadorProfesores++;

			this.mostrarProfes();
		} else {
			System.out.println("Profesores Completos");
		}

	}

	public void crearAlumnos() {

		if (contadorAlumnos <= alumnos.length) {

			System.out.println("Creando alumnos ..");
			System.out.println("Introduce nombre");
			String nombre = sc.next();
			System.out.println("Introduce apellidos");
			String apellidos = sc.next();
			System.out.println("Introduce sexo");
			String sexo = sc.next();
			System.out.println("Introduce edad");
			int edad = sc.nextInt();
			System.out.println("Introduce dni");
			String dni = sc.next();
			this.alumnos[contadorAlumnos] = new Alumnos(nombre, apellidos, sexo, edad, dni);
			contadorAlumnos++;
		} else {
			System.out.println("Alumnos Completos");
		}
	}

	public void vincularAsignaturas() {
		System.out.println("Selecciona que quieres vincular: ..\n");
		System.out.println(
				" 1.- Profesores y Asignaturas\n 2.- Alumnos y Asignaturas\n 3.- Volver al menú anterior.\n Escribe tu selección y pulsa la tecla INTRO");

		int mensajeVincular = sc.nextInt();

		if (mensajeVincular == 1) {
			this.vincularProfesAsignaturas();
		}
		if (mensajeVincular == 2) {
			this.vincularAlumnosAsignaturas();
		} else {
			this.run();
		}

	}

	public void vincularProfesAsignaturas() {

		System.out.println("Introduce dni del profesor ..");
		String mensajeDni = sc.next();
		for (int i = 0; i < profesores.length; i++) {

			if (mensajeDni.equals(this.profesores[i].getDni())) {
				String nombre = this.profesores[i].getNombre();
				String apellidos = this.profesores[i].getApellidos();
				this.asignarAsignaturaProfesor(nombre, apellidos);

			} else {
				System.out.println("Lo siento. No existe este dni " + mensajeDni + " de profesor");
				this.vincularAsignaturas();
			}

		}

	}

	public void asignarAsignaturaProfesor(String _nombre, String _apellidos) {
		System.out.println("Vinculando asignaturas a" + _nombre + "" + _apellidos);
		System.out.println("Indique las siglas de la asignatura a vincular. VOLVER si desea volver al menú anterior");
		String mensajeAsignatura = sc.next();
		if (mensajeAsignatura == "VOLVER") {
			this.vincularProfesAsignaturas();
		} else {
			for (int i = 0; i < asignaturas.length; i++) {
				if (mensajeAsignatura.equals(asignaturas[i].getSiglas())) {

				} else {
					System.out.println("Lo siento. No existe esta Asignatura " + mensajeAsignatura + "");
					this.asignarAsignaturaProfesor(_nombre, _apellidos);
				}

			}

		}
	}

	public void vincularAlumnosAsignaturas() {

		System.out.println("Introduce dni del alumno ..");
		String mensajeDni = sc.next();
		for (int i = 0; i < alumnos.length; i++) {

			if (mensajeDni.equals(this.alumnos[i].getDni())) {
				String nombre = this.alumnos[i].getNombre();
				String apellidos = this.alumnos[i].getApellidos();
				this.asignarAsignaturaAlumno(nombre, apellidos);

			} else {
				System.out.println("Lo siento. No existe este dni " + mensajeDni + " de alumno");
				this.vincularAsignaturas();

			}

		}

	}

	public void asignarAsignaturaAlumno(String _nombre, String _apellidos) {

		System.out.println("Vinculando asignaturas a" + _nombre + "" + _apellidos);
		System.out.println("Indique las siglas de la asignatura a vincular. VOLVER si desea volver al menú anterior");
		String mensajeAsignatura = sc.next();
		if (mensajeAsignatura == "VOLVER") {
			this.vincularAlumnosAsignaturas();
		} else {
			for (int i = 0; i < asignaturas.length; i++) {

				if (mensajeAsignatura.equals(asignaturas[i].getSiglas())) {

				}

				else {
					System.out.println("Asignatura NO VALIDA");
					this.asignarAsignaturaAlumno(_nombre, _apellidos);

				}

			}

		}

	}

	public void salir() {

	}

}
