package EjercicoColegio;

public class Asignaturas {
	
	private String nombre;
	private String siglas;
	private int horasLectivas;
	
	public Asignaturas(String _nombre, String _siglas, int _horasLectivas) {
		this.nombre = _nombre;
		this.siglas = _siglas;
		this.horasLectivas = _horasLectivas;
	}
	public String getSiglas() {
		return siglas;
	}
}
