package EjercicoColegio;

public class Alumnos extends Profesores {
	private String nombre;
	private String apellidos;
	private String sexo;
	private int edad;
	private String dni;
	private Asignaturas[] asignaturas_alumno;


	public Alumnos(String _nombre, String _apellidos, String _sexo, int _edad, String _dni) {
		super(_nombre, _apellidos, _sexo, _edad, _dni);
		this.asignaturas_alumno = new Asignaturas[10];	
	}

	public String getDni() {
		return dni;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	

}
