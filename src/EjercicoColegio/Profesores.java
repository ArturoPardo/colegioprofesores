package EjercicoColegio;

public class Profesores {

	private String nombre;
	private String apellidos;
	private String sexo;
	private int edad;
	private String dni;
	private Asignaturas[] asignaturas_profe;

	public Profesores(String _nombre, String _apellidos, String _sexo, int _edad, String _dni) {

		this.nombre = _nombre;
		this.apellidos = _apellidos;
		this.sexo = _sexo;
		this.edad = _edad;
		this.dni = _dni;
		this.asignaturas_profe = new Asignaturas[5];

	}

	public void anyadirAsignatura(Asignaturas _siglas) {
		this.asignaturas_profe[0] = _siglas;
	}

	public void MostrarArray() {
		System.out.println(this.asignaturas_profe.length);
	}

	public String getApellidos() {
		return apellidos;

	}

	public String getNombre() {
		return nombre;

	}

	public String getDni() {
		return dni;

	}

}
